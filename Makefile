install-dep:
	npm install

build: install-dep
	rm -rf lib
	npm run build

run-compiled:
	node lib/src/index.js

import { Cli } from './cli';


const cli = new Cli(process.argv);

console.time('compute');
cli.run();
console.timeEnd('compute');

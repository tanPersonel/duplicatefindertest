const getNumber = (num: number | string): number | undefined => {
  const convertToNumber = Number(num);

  if (convertToNumber) {
    return convertToNumber;
  }

  return undefined;
};

export { getNumber };

import { DuplicatePrinter } from './duplicatePrinter';
import { getNumber } from './utils/convertNumber';


const helps = `
  --help                print help screen
  
  --item-count          quantity of items(default: 1000000)
  --duplicate-count     quantity of duplicate items(default: 10)
`;

interface CLI {
  hasValue(type: string): boolean;
  getArgByType(type: Command): number | string | undefined;
  run(): void;
}
type Command = '--duplicate-count' | '--item-count';
type Prop = 'itemCount' | 'duplicateCount';


export class Cli implements CLI {

  itemCount?: string | number;
  duplicateCount?: string | number;
  _args: number[] | any;
  printerInstance: any;

  constructor(args: string[]) {
    const [_, __, ...rest] = args;
    this._args = rest || [];
    this.itemCount = this.getArgByType('--item-count');
    this.duplicateCount = this.getArgByType('--duplicate-count');
  }

  get hasHelp(): boolean {
    return (this._args.length < 1 || this._args.includes('--help'));
  }

  hasValue(type: Prop): boolean {
    return typeof this[type] !== 'string' && typeof this[type] !== undefined;
  }

  getArgByType(type: Command): number | string | undefined {
    if (this._args.includes(type)) {
      const index = this._args.indexOf(type);
      const value = this._args[index + 1];

      if (!value) {
        return `${type} of undefined value`;
      }

      return getNumber(value);
    }
    return `${type} of undefined value`;
  }

  run(): void {
    if (this.hasHelp) { return console.log(helps); }
    const results: { itemCount?: any, duplicateCount?: any } = {};

    if (this.hasValue('itemCount')) {
      results.itemCount = this.itemCount;
    }
    if (this.hasValue('duplicateCount')) {
      results.duplicateCount = this.duplicateCount;
    }

    if (!this.printerInstance) {
      this.printerInstance = new DuplicatePrinter(
        results.itemCount,
        results.duplicateCount
      );
    }

    return this.printerInstance.print();
  }
}

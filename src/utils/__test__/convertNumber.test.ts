import { getNumber } from '../convertNumber';

describe('Utils getNumber', () => {
  test('pass string number and return number', () => {
    expect(getNumber('11')).toBe(11);
  });

  test('pass alphanumeric and return undefined', () => {
    expect(getNumber('11as')).toBe(undefined);
  });

  test('pass number and return number', () => {
    expect(getNumber(11)).toBe(11);
  });
});

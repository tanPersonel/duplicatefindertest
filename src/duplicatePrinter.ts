
interface DuplicatePrinterInt {
  print(): void
}

export class DuplicatePrinter implements DuplicatePrinterInt{

  private static generateArrayWithDuplicate(itemCount: number, duplicateCount: number): number[] {
    const notDuplicateInts = Array.from(
      Array(itemCount).keys(),
      (d: number) => d + 1
    );

    for (let i = 0; i < duplicateCount; i++) {
      const random = Math.floor(Math.random() * itemCount) + 1;
      notDuplicateInts.push(random);
    }

    return notDuplicateInts;
  }

  n: number[];
  duplicates?: number[];

  // ::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::: CONSTRUCT
  constructor(N: number = 1000000, duplicateCount: number = 10) {
    this.n = DuplicatePrinter.generateArrayWithDuplicate(N, duplicateCount);
  }

  print() {
    // use cache if available
    if (this.duplicates) {
      return console.log(this.duplicates);
    }

    const { duplicates } = this.n.reduce(
      (acc: any, cur: number) => {
        if (acc.map[cur]) {
          acc.duplicates = [...acc.duplicates, cur];
          return acc;
        }
        acc.map[cur] = cur;
        return acc;
      },
      { duplicates: [], map: {} }
    );

    this.duplicates = duplicates;

    return console.log(this.duplicates);
  }
}

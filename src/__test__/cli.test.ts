import { Cli } from '../cli';

const commonArgs = ['node', 'path'];

describe('Cli', () => {
  const scenariosInitialize = [
    {
      name: 'initialize without argument',
      arg: [],
      expected: {
        itemCount: `--item-count of undefined value`,
        duplicateCount: `--duplicate-count of undefined value`
      }
    },
    {
      name: 'initialize with --item-count',
      arg: ['--item-count', '10'],
      expected: {
        itemCount: 10,
        duplicateCount: `--duplicate-count of undefined value`
      }
    },
    {
      name: 'initialize with --duplicate-count ',
      arg: ['--duplicate-count', '10'],
      expected: {
        itemCount: `--item-count of undefined value`,
        duplicateCount: 10
      }
    },
    {
      name: 'initialize with --duplicate-count and --item-count',
      arg: ['--duplicate-count', '10', '--item-count', '10'],
      expected: {
        itemCount: 10,
        duplicateCount: 10
      }
    }
  ];

  scenariosInitialize.forEach(({ name, arg, expected }) => {
    test(name, () => {
      const dup = new Cli([...commonArgs, ...arg]);

      expect(dup._args).toEqual(arg);
      expect(dup.itemCount).toBe(expected.itemCount);
      expect(dup.duplicateCount).toBe(expected.duplicateCount);
    });
  });

  const scenariosHasHelp = [
    { name: 'no argument', state: [], expected: true },
    { name: '--help', state: ['--help'], expected: true },
    {
      name: 'mix argument and --help',
      state: ['--item-count', '--help'],
      expected: true
    },
    {
      name: 'mix argument without --help',
      state: ['--item-count', '10', '--duplicate-count'],
      expected: false
    }
  ];

  scenariosHasHelp.forEach(({ name, expected, state }) => {
    test(`hasValue method with ${name}`, () => {
      const dup = new Cli([...commonArgs, ...state]);

      expect(dup.hasHelp).toBe(expected);
    });
  });

  const scenariosHasValue = [
    {
      name: 'itemCount string ',
      state: [],
      type: 'itemCount',
      expected: false
    },
    {
      name: 'duplicateCount string ',
      state: [],
      type: 'duplicateCount',
      expected: false
    },
    {
      name: 'itemCount has number ',
      state: ['--item-count', '10'],
      type: 'itemCount',
      expected: true
    },
    {
      name: 'duplicateCount has number ',
      state: ['--duplicate-count', '10'],
      type: 'duplicateCount',
      expected: true
    }
  ];

  scenariosHasValue.forEach(({ name, expected, state, type }) => {
    test(`hasValue with ${name}`, () => {
      const dup = new Cli([...commonArgs, ...state]);
      console.log(`type`, type);

      switch (type) {
        case 'duplicateCount':
          expect(dup.hasValue('duplicateCount')).toBe(expected);
          break;

        default:
          expect(dup.hasValue('itemCount')).toBe(expected);
          break;
      }
    });
  });
});

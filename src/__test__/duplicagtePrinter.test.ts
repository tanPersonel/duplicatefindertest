import { DuplicatePrinter } from '../duplicatePrinter';

const noop = () => {};

describe('DuplicatePrinter', () => {
  test('initialize without argument and has n property', () => {
    const dup = new DuplicatePrinter();

    expect(dup.hasOwnProperty('n')).toBe(true);
  });

  test('default return 10 duplicate value', () => {
    const oldConsoleLog = console.log;
    console.log = noop;
    const dup = new DuplicatePrinter();
    dup.print();
    const expected = dup.duplicates || [];
    console.log = oldConsoleLog;
    expect(expected.length).toBe(10);
  });

  test('return 4 duplicate value', () => {
    const dup = new DuplicatePrinter(10, 4);
    const oldConsoleLog = console.log;
    console.log = noop;
    dup.print();
    const expected = dup.duplicates || [];
    console.log = oldConsoleLog;
    expect(expected.length).toBe(4);
  });

  test('duplicates cache undefined', () => {
    const dup = new DuplicatePrinter(10, 4);
    const expected = dup.duplicates;
    expect(expected).toBe(undefined);
  });

  test('duplicates cached', () => {
    const dup = new DuplicatePrinter(10, 4);
    const oldConsoleLog = console.log;
    console.log = noop;
    dup.print();
    dup.print();
    console.log = oldConsoleLog;
    const expected = Boolean(dup.duplicates);
    expect(expected).toBe(true);
  });
});

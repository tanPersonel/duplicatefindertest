## Duplicate Finder

First compile ts. 
``` node
  make build
```
##
Now you can use the cli to get the duplicates
``` node
  node lib/src/index.js --help
  node lib/src/index.js --item-count 1000000 --duplicate-count 20
```
##
Run tests
``` node
  npm run test
```
